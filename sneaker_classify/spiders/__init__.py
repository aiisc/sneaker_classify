import scrapy
from pathlib import Path
from inline_requests import inline_requests
from pprint import pprint
import random

class Sneaker(scrapy.Spider):
	name = "classify"
	data = {
		"nike_dunks": [],
		"jordan_ones": []
	}

	
	def start_requests(self):
		path = Path(__file__).parent / "done.txt"
		with path.open() as file:
			urls = file.readlines()
		print("Classifying first 20 images...")
		headers = [
			"Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30",
			"Mozilla/5.0 (Linux; U; Android 2.3.3; zh-tw; HTC_Pyramid Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1",
			"Googlebot-Image/1.0",
			"Mozilla/5.0 (Linux; U; Android 2.2; en-sa; HTC_DesireHD_A9191 Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1"
		]

		for url in urls[::-1]:

			id_ = url.split("/")[-1].split("_")[0]
			page = f"https://www.yoox.com/us/{id_}/item#cod10=&sizeId=-1"
			yield scrapy.Request(
				page,
				headers={
				    "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
					"accept-encoding": "gzip, deflate, br",
					"accept-language": "en-US,en;q=0.9,sv;q=0.8,hy;q=0.7",
					"user-agent": random.choice(headers)
				},
				callback=self.predict,
				meta={'url':url}
			)

	def predict(self,response):
		url = response.meta.get('url')
			# id_ = url.split("/")[-1].split("_")[0]
		try:
			search_fields = response.xpath('/html/head/meta[26]/@content').get().lower()
		except:
			search_fields = ""
		try:
			title = response.xpath('/html/head/title/text()').get().lower()
		except:
			title = ""

		if "nike dunk" in title or "dunk" in title:
			yield {"nike": url}
		elif "nike dunk" in search_fields or "dunk" in search_fields:
		        yield {"nike": url}
		elif "jordan 1" in title or "jordan 1" in search_fields:
			yield {"jordan": url}
		elif "jordan" in title or "jordan" in search_fields:
			yield {"jordan": url}
		else:
			yield {"none": url}
